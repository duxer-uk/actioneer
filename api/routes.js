// routes.js

// Initialize express router
let router = require('express').Router();

// Import auction controller
var auctionController = require('./auctionController');

// Set default API response
router.get('/', function (req, res) {
    res.json({
        status: "success",
        message: 'endpoint working!'
    });
});


// auction routes
router.route('/list')
    .get(auctionController.index)
    .post(auctionController.new);
router.route('/list/:auction_id')
    .get(auctionController.view)
    .patch(auctionController.update)
    .put(auctionController.update)
    .delete(auctionController.delete);
router.route('/scraper')
    .post(auctionController.scrape);    

 // Export API routes
module.exports = router;