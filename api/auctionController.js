// auctionController.js
// Import auction model

let Auction = require('../models/model'),
    scraper = require('../services/scraper');

// Handle index actions
exports.index = function (req, res) {
    Auction.get(function (err, auctions) {
        if (err) {
            res.json({
                status: "error",
                message: err,
            });
        }
        res.json({
            status: "success",
            message: "Auctions retrieved successfully",
            data: auctions
        });
    });
};

// Handle create auction
exports.new = function (req, res) {
    var auction = new Auction();
    auction.name = req.body.name ? req.body.name : auction.name;
    auction.gender = req.body.gender;
    auction.email = req.body.email;
    auction.phone = req.body.phone;


// save the auction and check for errors
    auction.save(function (err) {
            // if (err)
            //     res.json(err);
    res.json({
                message: 'New auction imported!',
                data: auction
            });
        });
    };

    // Handle create auction
exports.scrape = function (req, res) {

    res.status(204).send(

        scraper(req.body.auctionId)
    )    
};

// Handle view auction info
exports.view = function (req, res) {
    Auction.findById(req.params.auction_id, function (err, auction) {
        if (err)
            res.send(err);
        res.json({
            message: 'Auction details',
            data: auction
        });
    });
};

// Handle update auction info
exports.update = function (req, res) {
Auction.findById(req.params.auction_id, function (err, auction) {
        if (err)
            res.send(err);
        auction.name = req.body.name ? req.body.name : auction.name;
        auction.gender = req.body.gender;
        auction.email = req.body.email;
        auction.phone = req.body.phone;
    
        // save the auction and check for errors
        auction.save(function (err) {
            if (err)
                res.json(err);
            res.json({
                message: 'auction updated',
                data: auction
            });
        });
    });
};

// Handle delete auction
exports.delete = function (req, res) {
    Auction.remove({
        _id: req.params.auction_id
    }, function (err, auction) {
        if (err)
            res.send(err);
res.json({
            status: "success",
            message: 'Auction deleted'
        });
    });
};