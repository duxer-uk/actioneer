// auctionModel.js
var mongoose = require('mongoose');

// Setup schema
var auctionSchema = mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    gender: String,
    phone: String,
    create_date: {
        type: Date,
        default: Date.now
    }
});

// Export Auction model
var Auction = module.exports = mongoose.model('auction', auctionSchema);

module.exports.get = function (callback, limit) {
    Auction.find(callback).limit(limit);
}