
var html2json = require('html2json').html2json;

function scraper(_url, _id, _domain) {
  const got = require('got');
  const jsdom = require("jsdom");
  const { JSDOM } = jsdom;

  let url = _url,
      id = _id,
      domain = _domain,
      index = 1,
      html,
      json; 

      url += "&pageno=" + index;
      // console.log(url + " " + id + " " + domain);
  
  // const isMidi = (link) => {
  //   // Return false if there is no href attribute.
  //   if (typeof link.href === 'undefined') { return false }

  //   return link.href.includes('.mid');
  // };

  // const noParens = (link) => {
  //   // Regular expression to determine if the text has parentheses.
  //   const parensRegex = /^((?!\().)*$/;
  //   return parensRegex.test(link.textContent);
  // };

    (async () => {
      const response = await got(url);
      const dom = new JSDOM(response.body);

      // Create an Array out of the HTML Elements for filtering using spread syntax.
      const nodeList = [...dom.window.document.querySelectorAll('.blacklink2')];
      

      nodeList.forEach(node => { //filter(isMidi).filter(noParens).
        
        html += node.outerHTML;
      });

    })();
  
  json = html2json(html);
  console.log(json);
}

module.exports = scraper;