// index.js

/** Required Modules */

const http = require('http'),
    express = require('express'),
    mongoose = require('mongoose'),
    cors = require('cors'),
    bodyParser = require('body-parser'),
    database = require('./config/database'),
    path = require('path');

const { JSDOM } = require( "jsdom" ),
      { window } = new JSDOM( "" ),
      $ = require("jquery")(window);

    
// Import routes
let routes = require("./api/routes"),
    scraper = require("./services/scraper");
const app = express(),
      port = process.env.PORT || "8000",
      server = http.createServer(app);

      
// Configure bodyparser to handle post requests

// app.use(express.static("express"));
// app.use(express.json());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(cors());
app.use(express.static(path.join(__dirname, 'public')));
app.use('/auction', routes);

app.post('/scraper', function (req, res) {

    res.status(200).send(scraper(req.body.auctionUrl, req.body.auctionId, req.body.domain)); 

});


// Connecting mongoDB
mongoose.Promise = global.Promise;
mongoose.connect(database.db, {
    useNewUrlParser: true,
    useUnifiedTopology: true
}).then(() => {
        console.log('database connected')
    },
    error => {
        console.log('database could not be connected : ' + error)
    }
)

// mongoose.connect('mongodb://localhost/actioneer', { useNewUrlParser: true, useUnifiedTopology: true});
// var db = mongoose.connection;

// // Added check for DB connection
// if(!db)
//     console.log("Error connecting db")
// else
//     console.log("Db connected successfully")



/** Server */

app.use('/', function(req,res){
    res.sendFile(path.join(__dirname + '/public/index.html'));
});

server.listen(port, () => {
    console.log(`listening on :${port}`);
}); 